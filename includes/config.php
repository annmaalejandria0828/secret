<?php
    $db_host        = "127.0.0.1";
    $db_name        = "api_db";
    $db_username    = "root";
    $db_password    = "";

    try {
        
        $db = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8", $db_username, $db_password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "Connection successful";

    } catch(PDOException $e) {

        echo "Connection failed: " . $e->getMessage();
    }
?>