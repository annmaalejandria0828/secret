<?php

    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Authorization, X-Requested-With");

    include_once("src/initialize.php");

    $user = new User($db);
    $mykey = basename($_SERVER['PHP_SELF']);
    // print_r($mykey);
    
    if(isset($_GET['timestamp']))
    {
        $timestamp = $_GET['timestamp'];
        getUserTime($user, $timestamp);
    }
    elseif ($mykey != 'get_all_records' && $mykey != 'users.php')
    {
        getUser($user, $mykey);
    }
    elseif ($mykey == 'get_all_records')
    {
        getAllUsers($user);
    }
    else {
        postUser($user);
    }

    function postUser($user)
    {
        $data = json_decode(file_get_contents("php://input"));

        if(isset($data))
        {
            $user->name         = $data->name;
            $user->description  = $data->description;

            if($user->postUser())
            {
                echo json_encode(array('message' => 'User created.'));
            }
            else {
                echo json_encode(array('message' => 'User not created.'));
            }
        }
        else {
            echo json_encode(array('message' => 'User not created.'));
        }

    }

    function getUserTime($user, $timestamp)
    {
        $user->created_at = gmdate('Y-m-d H:i:s', $timestamp);
        $result = $user->getUserTime();
        if($result->rowCount() > 0)
        {
            $user_array = array();
            $user_array['data'] = array();

            while($data = $result->fetch(PDO::FETCH_ASSOC))
            {
                extract($data);
                $user = array(
                    'id'            => $id,
                    'title'         => $name,
                    'description'   => $description,
                    'created_at'    => $created_at
                );
                array_push($user_array['data'], $user);
            }
            echo json_encode($user_array);
        }
        else {
            echo json_encode(array('message' => 'No user found with timestamp '.$timestamp.' ('.date('Y-m-d H:i:s', $timestamp).').'));
        }
    }

    function getUser($user, $mykey)
    {
        $user->id = $mykey;
        $result = $user->getUser();
        if($result->rowCount() == 1)
        {
            $data = $result->fetch(PDO::FETCH_ASSOC);
            $user_array = array(
                'id'            => $data['id'],
                'name'          => $data['name'],
                'description'   => $data['description'],
                'created_at'    => $data['created_at'],
            );

            print_r(json_encode($user_array));
        }
        else {
            echo json_encode(array('message' => 'No user found with key '.$mykey.'.'));
        }
    }

    function getAllUsers($user)
    {
        $result = $user->getAllUsers();
        if($result->rowCount() > 0)
        {
            $user_array = array();
            $user_array['data'] = array();

            while($data = $result->fetch(PDO::FETCH_ASSOC))
            {
                extract($data);
                $user = array(
                    'id'            => $id,
                    'title'         => $name,
                    'description'   => $description,
                    'created_at'    => $created_at
                );
                array_push($user_array['data'], $user);
            }
            echo json_encode($user_array);
        }
        else {
            echo json_encode(array('message' => 'No user(s) found.'));
        }
    }
