<?php


require('vendor/autoload.php');

use PHPUnit\Framework\TestCase;
/**
 * 
 * @covers User
 *
 */
class UnitTest extends TestCase
{
    protected $client;

    public function setUp() :void 
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://54.179.85.171/'
        ]);
    }

    /**
     * 
     * @return void
     */
    public function testGetAllUsers()
    {
        $response = $this->client->get('/users.php/get_all_records');

        $this->assertEquals($response->getStatusCode(), 200);

        $data = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * 
     * @return void
     */
    public function testGetUser()
    {
        $response = $this->client->get('/users.php/1');

        $this->assertEquals($response->getStatusCode(), 200);

        $data = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('created_at', $data);
    }

    /**
     * 
     * @return void
     */
    public function testGetUserTime()
    {
        $response = $this->client->get('/users.php/1?timestamp=1625076000');

        $this->assertEquals($response->getStatusCode(), 200);

        $data = json_decode($response->getBody(), true);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * 
     * @return void
     */
    public function testPostUser()
    {
        $response = $this->client->post('/users.php', [
            'json' => [
                'name'          => 'Test Name',
                'description'   => 'Test Description',
            ]
        ]);

        $this->assertEquals($response->getStatusCode(), 200);
    }
}
