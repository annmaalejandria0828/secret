<?php

class User {

    private $conn;
    private $table = 'users';

    public $id;
    public $name;
    public $description;
    public $created_at;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Query all Users
    public function getAllUsers()
    {
        $query = "SELECT id, name, description, created_at FROM ".$this->table;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    // Query User by id
    public function getUser()
    {
        $query = "SELECT id, name, description, created_at FROM ".$this->table." WHERE id = ? LIMIT 1";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        return $stmt;
    }

    // Query User by timestamp
    public function getUserTime()
    {
        $query = "SELECT id, name, description, created_at FROM ".$this->table." WHERE created_at <= ?";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->created_at);
        $stmt->execute();

        return $stmt;
    }

    // Create User
    public function postUser()
    {
        $query = "INSERT INTO ".$this->table." SET name = :name, description = :description";

        $stmt = $this->conn->prepare($query);
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->description = htmlspecialchars(strip_tags($this->description));

        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":description", $this->description);

        if($stmt->execute())
        {
            return true;
        }
        printf("Error $s. \n", $stmt->error);
        return false;
    }
}
